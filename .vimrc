" https://www.reddit.com/r/vim/comments/49uag0/vundle_vs_pathogen_vs/

" https://github.com/tpope/vim-pathogen
" execute pathogen#infect()

" !!! gonna use vim-plug instead !!!

" Specify a directory for plugins
" https://github.com/junegunn/vim-plug
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

Plug 'autozimu/LanguageClient-neovim', { 'branch': 'next', 'do': 'bash install.sh' }

Plug 'https://github.com/igravious/unicode.vim'

Plug 'https://github.com/posva/vim-vue'

Plug 'https://github.com/vim-ruby/vim-ruby'

" Initialize plugin system
call plug#end()

" Required for operations modifying multiple buffers like rename.
set hidden

    " \ 'ruby': ['~/.rbenv/shims/solargraph', 'stdio'],
let g:LanguageClient_serverCommands = {
    \ 'ruby': ['~/.gem/ruby/'.$RUBY_SERIES.'/bin/solargraph', 'stdio'],
    \ }

nnoremap <F5> :call LanguageClient_contextMenu()<CR>
" Or map each action separately
nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>

" Microsoft Intellisense mapping :)
inoremap <Nul> <C-x><C-o>

syntax on
" how to do this only for Ruby?

" https://stackoverflow.com/questions/1562633/setting-vim-whitespace-preferences-by-filetype
autocmd Filetype ruby       setlocal noexpandtab ts=2 sts=2 sw=2
autocmd FileType ruby       setlocal omnifunc=LanguageClient#complete

set background=light

set hlsearch

" screen is plently wide to show numbers
" set number

" associate *.hbs with html filetype
au BufRead,BufNewFile *.hbs setfiletype html

if &diff
    " diff mode
    set diffopt+=iwhite
endif

" set paste

autocmd FileType html       setlocal noexpandtab shiftwidth=2 tabstop=2
autocmd FileType css        setlocal noexpandtab shiftwidth=2 tabstop=2
autocmd FileType javascript setlocal noexpandtab shiftwidth=2 tabstop=2
autocmd FileType ruby       setlocal noexpandtab shiftwidth=2 tabstop=2
autocmd FileType eruby      setlocal noexpandtab shiftwidth=2 tabstop=2
autocmd FileType vue        setlocal noexpandtab shiftwidth=2 tabstop=2

" for Insight Centre codebase
autocmd FileType java        setlocal expandtab shiftwidth=4 tabstop=4
autocmd FileType xml         setlocal expandtab shiftwidth=4 tabstop=4

function! ZoteroCite()
  " pick a format based on the filetype (customize at will)
  let format = &filetype =~ '.*tex' ? 'citep' : 'pandoc'
  let api_call = 'http://127.0.0.1:23119/better-bibtex/cayw?format='.format.'&brackets=1'
  let ref = system('curl -s '.shellescape(api_call))
  return ref
endfunction

noremap <leader>z "=ZoteroCite()<CR>p
inoremap <C-z> <C-r>=ZoteroCite()<CR>

noremap <silent> <Leader>w :call ToggleWrap()<CR>
function ToggleWrap()
  if &wrap
    echo "Wrap OFF"
    setlocal nowrap
    set virtualedit=all
    silent! nunmap <buffer> <Up>
    silent! nunmap <buffer> <Down>
    silent! nunmap <buffer> <Home>
    silent! nunmap <buffer> <End>
    silent! iunmap <buffer> <Up>
    silent! iunmap <buffer> <Down>
    silent! iunmap <buffer> <Home>
    silent! iunmap <buffer> <End>
  else
    echo "Wrap ON"
    setlocal wrap linebreak list
    set virtualedit=
    setlocal display+=lastline
    noremap  <buffer> <silent> <Up> gk
    noremap  <buffer> <silent> <Down> gj
    noremap  <buffer> <silent> <Home> g<Home>
    noremap  <buffer> <silent> <End> g<End>
    inoremap <buffer> <silent> <Up> <C-o>gk
    inoremap <buffer> <silent> <Down> <C-o>gj
    inoremap <buffer> <silent> <Home> <C-o>g<Home>
    inoremap <buffer> <silent> <End> <C-o>g<End>
  endif
endfunction
