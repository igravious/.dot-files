# http://stackoverflow.com/questions/10929453/bash-scripting-read-file-line-by-line
# http://www.forensicswiki.org/wiki/Pdfinfo

#!/bin/bash
while IFS='' read -r line || [[ -n "$line" ]]; do
        echo "$line"
        NAME=$(basename "$line" .pdf)
        DIR=$(dirname "$line")
        echo "$NAME"
        echo "$DIR"
        echo "$line" > ./Documents/PDFs/"$NAME".txt
        echo "--------" >> ./Documents/PDFs/"$NAME".txt
        pdfinfo "$line" >> ./Documents/PDFs/"$NAME".txt
done < "$1"
