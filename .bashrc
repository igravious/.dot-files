# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    source ~/.bash_prompt.sh
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

path_add_if ()  { if [ -d "$1" ] && [[ ":$PATH:" != *":$1:"* ]]; then PATH="${PATH:+"$PATH:"}$1"; fi }
path_remove ()  { export PATH=`echo -n $PATH | awk -v RS=: -v ORS=: '$0 != "'$1'"' | sed 's/:$//'`; }
path_append ()  { path_remove $1; export PATH="$PATH:$1"; }
path_prepend () { path_remove $1; export PATH="$1:$PATH"; }

source ~/.bash_$(hostname).sh

# set PATH so it includes user's private rbenv bin if it exists
path_add_if $HOME/.rbenv/bin
#if [ -d "$HOME/.rbenv/bin" ] ; then
#    PATH="$HOME/.rbenv/bin:$PATH"
#fi

eval "$(rbenv init -)"

# look how Node.js does it
path_add_if $(npm bin)
#if [ -d "$(npm bin)" ] ; then
#    PATH="$(npm bin):$PATH"
#fi

# where i've put saffron.git
path_add_if $HOME/Code/gitlab_saffron.git
#if [ -d "$HOME/Code/gitlab_saffron.git" ] ; then
#    PATH="$HOME/Code/gitlab_saffron.git:$PATH"
#fi

alias bundle-install='bundle install --path vendor/cache --clean --jobs 2'

# https://stackoverflow.com/questions/10781824/export-not-working-in-my-shell-script
# duh

source ~/bin/gem_bin
alias gem-install='gem --user-install install'

# https://stackoverflow.com/questions/1911861/how-to-fix-the-sml-nj-interactive-system-to-use-arrow-keys
alias sml='rlwrap sml'
alias mosml='rlwrap mosml'

[ -s "/home/groobiest/.dnx/dnvm/dnvm.sh" ] && . "/home/groobiest/.dnx/dnvm/dnvm.sh" # Load dnvm

export PYTHONSTARTUP=~/.pythonrc

echo -e "\e[1mCtrl + Super + Up\e[0m     Maximises the current window."
echo -e "\e[1mF9\e[0m                    Launch byobu-config window (if byobu in use)."

# added by Anaconda3 4.4.0 installer
# unadded by me, overrides sqlite3 :(
# export PATH="/home/groobiest/anaconda3/bin:$PATH"

xhost +si:localuser:root

alias rfind='find . -type d \( -name vendor -o -name node_modules -o -name tmp -o -name .git \) -prune -o'

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/groobiest/Code/CImg/google-cloud-sdk/path.bash.inc' ]; then source '/home/groobiest/Code/CImg/google-cloud-sdk/path.bash.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/home/groobiest/Code/CImg/google-cloud-sdk/completion.bash.inc' ]; then source '/home/groobiest/Code/CImg/google-cloud-sdk/completion.bash.inc'; fi

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

LS_COLORS=

GPG_TTY=$(tty)
export GPG_TTY

alias config='/usr/bin/git --git-dir=$HOME/.dot-files.git/ --work-tree=$HOME'
