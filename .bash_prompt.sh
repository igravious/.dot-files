
# begone

# PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"

# https://unix.stackexchange.com/questions/28827/why-is-my-bash-prompt-getting-bugged-when-i-browse-the-history
# \[…\] :(

Default='\[\033[0m\]'
Cya='\[\e[0;36m\]'
BIPur='\[\e[1;95m\]'

# PS1="\u @ \h $Cyan\w $LightRed$(__git_ps1)\n$Default▶ "

PS1="\u@\h $Cya\w\$(__git_ps1 '$BIPur [%s]') $Default➤ "
